package id.sch.smktelkommlg.www.belajarukl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private Button reg, log;
    private EditText user, pass;
    private dbHelper db;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new dbHelper(this);
        reg = findViewById(R.id.daftar);
        log = findViewById(R.id.login);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);
        log.setOnClickListener(this);
        reg.setOnClickListener(this);

//        if(session.loggedin()){
//            startActivity(new Intent(LoginActivity.this,MainActivity.class));
//            finish();
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                login();
                break;
            case R.id.daftar:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
            default:
        }
    }

    private void login() {
        String username = user.getText().toString();
        String password = pass.getText().toString();
        if (db.getUser(username, password)) {
//            session.setLoggedin(true);
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "WRONG", Toast.LENGTH_SHORT).show();
        }
    }
}
