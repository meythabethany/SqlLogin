package id.sch.smktelkommlg.www.belajarukl;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private Button logout;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

    }

    private void logout() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }

    public void loadUser(View view) {
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.user);
        Scanner scanner = new Scanner(is);
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

        parseJson(builder.toString());
    }

    private void parseJson(String s) {
//        TextView dispay = (TextView) findViewById(R.id.hitTEXT);
        StringBuilder builder = new StringBuilder();

        try {
            JSONObject root = new JSONObject(s);

            JSONArray users = root.getJSONArray("user");

            for (int i = 0; i < users.length(); i++) {
                JSONObject user = users.getJSONObject(i);
                builder.append("IDUser :  ").append(user.getInt("IDUser")).append("\n");
                builder.append("Name :  ").append(user.getString("Name")).append("\n");
                builder.append("Username :  ").append(user.getInt("UserName")).append("\n");
                builder.append("Password :  ").append(user.getInt("Password")).append("\n");
                builder.append("Branch :  ").append(user.getString("Branch")).append("\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//
//        dispay.setText(builder.toString());
    }
}
