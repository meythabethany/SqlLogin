package id.sch.smktelkommlg.www.belajarukl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button reg, log;
    EditText user, pass;
    dbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new dbHelper(this);
        reg = findViewById(R.id.daftar);
        reg.setOnClickListener(this);
        log = findViewById(R.id.login);
        log.setOnClickListener(this);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.daftar:
                daftar();
                break;
            case R.id.login:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                break;
            default:
        }
    }

    public void daftar() {

        String username = user.getText().toString();
        String password = pass.getText().toString();
        if (username.isEmpty() && password.isEmpty()) {
            displayToast("ISI USERNAME DAN PASSWORD");
        } else {
            db.addUser(username, password);
            displayToast("Berhasil");
            finish();
        }
    }

    private void displayToast(String pesan) {
        Toast.makeText(getApplicationContext(), pesan, Toast.LENGTH_SHORT).show();
    }
}
