package id.sch.smktelkommlg.www.belajarukl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Meytha Bethany Putri on 03/05/2018.
 */

public class dbHelper extends SQLiteOpenHelper {

    public static final String TAG = dbHelper.class.getSimpleName();

    public static final String DB_NAME = "belajarukl.db";
    public static final int DB_VERSION = 1;

    public static final String DB_TABLE = "user";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_USER = "username";
    public static final String COLUMN_PASS = "password";
    public static final String COLUMN_BRANCH = "branch";

    public static final String CREATE_USER = "CREATE TABLE " +
            DB_TABLE + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_USER + " TEXT, " +
            COLUMN_PASS + " TEXT, " +
            COLUMN_BRANCH + " TEXT);";

    public dbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format("DROP TABLE IF EXISTS", DB_TABLE));
        onCreate(db);
    }

    public void addUser(String user, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USER, user);
        contentValues.put(COLUMN_PASS, password);
        long id = db.insert(DB_TABLE, null, contentValues);
        db.close();

        Log.d(TAG, "user inserted" + id);
    }

    public boolean getUser(String user, String password) {

        String query = "select * from " + DB_TABLE + " where " + COLUMN_USER + " = " + "'" + user + "'" + " and " + COLUMN_PASS +
                " = " + "'" + password + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            return true;
        }
        cursor.close();
        db.close();

        return false;
    }
}
